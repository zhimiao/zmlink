package zmlink

import (
	"fmt"
	"testing"
	"time"

	"github.com/sirupsen/logrus"
)

type clientTestEventHandler struct{}

func (t clientTestEventHandler) OnStream(link *Link, payload Payload, dir StreamDir) {
	logrus.Infoln("OnStream-->", link.GetClientID(), string(payload.Bytes()), dir.String())
}

func (t clientTestEventHandler) OnStatus(link *Link, statusType StatusType) {
	logrus.Infoln("OnStatus-->", link.GetClientID(), statusType)
}

func TestClientLink_ASend(t *testing.T) {
	service := NewLinkClient("1235", "localhost:4469", &clientTestEventHandler{})
	// 默认断线后会自动重连
	_ = service.Start()
	for {
		time.Sleep(4 * time.Second)
		payload, _ := NewBytesPayload(1, []byte("22ZM222"))
		link := service.GetLink()
		if link == nil {
			logrus.Errorln("关闭了")
			continue
		}
		send, err := link.SSend(payload, 10*time.Second)
		if err != nil {
			logrus.Errorln(err.Error())
			continue
		}
		fmt.Println(string(send.Bytes()))
		// 异步回调没有超时时间，如果client端没有对这个报文定义返回，则会导致回调方法不断增加，直至oom
		err = link.ASend(payload, func(p Payload) {
			logrus.Infoln("clientDataHandler-->", string(p.Bytes()))
		})
		if err != nil {
			logrus.Errorln(err.Error())
			continue
		}
	}
}

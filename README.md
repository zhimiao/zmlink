## zmlink

一个定义了基本交互报文的tcp C/S端链接sdk，简化链接服务构建，使用方式如下方demo

#### 报文结构

注册帧(对使用者透明)

| ZM | 19 | 0 | 1 | clientID |

| 头   | 长度位    | 类型位  | 报文编号   | 载荷     |
|:----|:-------|:-----|:-------|:-------|
| ZM  | uint32 | int8 | uint64 | string |

> 数据存储类型采用大端字节序

> 类型位，载荷位用户自定义，但约定发送类型为1~126，Ack类型为对应的-1~-126



demo

> server端

```go
package zmlink

import (
	"testing"
)

type linstenerTestEventHandler struct{}

func (t linstenerTestEventHandler) OnStream(link *Link, payload Payload, dir StreamDir) {
	logrus.Infoln("OnStream-->", link.GetClientID(), string(payload.Bytes()), dir.String())
	if dir == StreamIncoming {
		link.ASend(payload.Ack([]byte("回复点消息拉卡拉绿绿绿绿绿绿绿")), nil)
	}
}

func (t linstenerTestEventHandler) OnStatus(link *Link, statusType StatusType) {
	logrus.Infoln("OnStatus-->", link.GetClientID(), statusType)
}

func TestLink_ASend(t *testing.T) {
	service := NewLinkListener("tcp", ":4469", &linstenerTestEventHandler{})
	service.Start()
	select {}
	/*for{
		time.Sleep(4 *time.Second)
		payload, _ := NewBytesPayload(1, []byte("hahahahah"))
		link := service.GetLink("1235")
		if link != nil {
			send, err := link.SSend(payload, 0)
			logrus.Infoln(string(send.Bytes()), err)
		}
	}*/
}
```

> client端

```go
package zmlink

import (
	"github.com/sirupsen/logrus"
	"testing"
)

type clientTestEventHandler struct{}

func (t clientTestEventHandler) OnStream(link *Link, payload Payload, dir StreamDir) {
	logrus.Infoln("OnStream-->", link.GetClientID(), string(payload.Bytes()), dir.String())
}

func (t clientTestEventHandler) OnStatus(link *Link, statusType StatusType) {
	logrus.Infoln("OnStatus-->", link.GetClientID(), statusType)
}

func TestClientLink_ASend(t *testing.T) {
	service := NewLinkClient("1235", "tcp", ":4469", &clientTestEventHandler{})
	service.Start()
	for {
		time.Sleep(4 * time.Second)
		payload, _ := NewBytesPayload(1, []byte("22ZM222"))
		link := service.GetLink()
		if link == nil {
			logrus.Errorln("关闭了")
			continue
		}
		send, err := link.SSend(payload, 10*time.Second)
		if err != nil {
			logrus.Errorln(err.Error())
			continue
		}
		fmt.Println(string(send.Bytes()))
		err = link.ASend(payload, func(p Payload) {
			logrus.Infoln("clientDataHandler-->", string(p.Bytes()))
		})
		if err != nil {
			logrus.Errorln(err.Error())
			continue
		}
	}
}
```
package zmlink

import (
	"fmt"
	"testing"
	"time"

	"github.com/sirupsen/logrus"
)

type linstenerTestEventHandler struct{}

func (t linstenerTestEventHandler) OnStream(link *Link, payload Payload, dir StreamDir) {
	logrus.Infoln("OnStream-->", link.GetClientID(), string(payload.Bytes()), dir.String())
	if dir == StreamIncoming {
		_ = link.ASend(payload.Ack([]byte("回复点消息拉卡拉绿绿绿绿绿绿绿")), nil)
	}
}

func (t linstenerTestEventHandler) OnStatus(link *Link, statusType StatusType) {
	logrus.Infoln("OnStatus-->", link.GetClientID(), statusType)
}

func TestLink_ASend(t *testing.T) {
	service := NewLinkListener(":4469", &linstenerTestEventHandler{})
	_ = service.Start()
	for {
		time.Sleep(4 * time.Second)
		payload, _ := NewBytesPayload(1, []byte("Server->发送的biubiubiu"))
		link := service.GetLink("1235")
		if link == nil {
			logrus.Errorln("关闭了")
			continue
		}
		send, err := link.SSend(payload, 10*time.Second)
		if err != nil {
			logrus.Errorln(err.Error())
			continue
		}
		fmt.Println(string(send.Bytes()))
		// 异步回调没有超时时间，如果client端没有对这个报文定义返回，则会导致回调方法不断增加，直至oom
		err = link.ASend(payload, func(p Payload) {
			logrus.Infoln("clientDataHandler-->", string(p.Bytes()))
		})
		if err != nil {
			logrus.Errorln(err.Error())
			continue
		}
	}
}

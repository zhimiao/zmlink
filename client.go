package zmlink

import (
	"net"
	"sync"
)

type Client struct {
	clientID          string
	Type              string // tcp4/udp
	Address           string // listen address
	linkEventListener LinkEventListener
	link              *Link
	sync.Mutex        // conneceted 锁
}

func NewLinkClient(clientID, address string, linkEventListener LinkEventListener) *Client {
	l := &Client{
		clientID:          clientID,
		Type:              "tcp",
		Address:           address,
		linkEventListener: linkEventListener,
	}
	return l
}

func (c *Client) Start() error {
	tcpAddr, err := net.ResolveTCPAddr(c.Type, c.Address)
	if err != nil {
		return err
	}
	conn, err := net.DialTCP(c.Type, nil, tcpAddr)
	if err != nil {
		return err
	}
	c.link = newLink(conn, c)
	c.link.SetClientID(c.clientID)
	err = c.link.run()
	if err != nil {
		return err
	}
	// 注册帧识别
	if err := c.link.sendRegister(); err != nil {
		c.link.Close()
		return err
	}
	c.link.fireStatusEvent(StatusReady)
	return nil
}

// GetLink 获取连接
func (c *Client) GetLink() *Link {
	if c.link.status == StatusReady {
		return c.link
	}
	return nil
}

func (c *Client) OnStream(link *Link, payload Payload, dir StreamDir) {
	c.linkEventListener.OnStream(link, payload, dir)
}

func (c *Client) OnStatus(link *Link, statusType StatusType) {
	if statusType == StatusClosed {
		c.reconnect()
	}
	c.linkEventListener.OnStatus(link, statusType)
}

func (c *Client) reconnect() {
	for {
		if err := c.Start(); err == nil {
			break
		}
	}
}

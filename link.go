package zmlink

import (
	"encoding/binary"
	"errors"
	"fmt"
	"net"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
)

const (
	WriteInterval     = 200 * time.Millisecond
	KeepAliveTimeout  = 60 * time.Minute // 2*time.Minute + 2*time.Second
	linkWriteTimeout  = 10 * time.Second
	readWaitTime      = 100 * time.Millisecond
	reconnectWaitTime = 5 * time.Second
)

type LinkDataHandler struct {
	ack      bool
	pkgType  int8
	pkgIndex uint64
	fn       func(payload Payload)
}

// StatusType 状态类型
type StatusType byte

const (
	// StatusInit 初始(Conn已实例化)
	StatusInit StatusType = iota
	// StatusError 错误(有异常)
	StatusError
	// StatusReady 就绪(可用)
	StatusReady
	// StatusClosed 已关闭
	StatusClosed
)

// StatusTypeString 状态(s)转字串
func (s StatusType) String() string {
	switch s {
	case StatusReady:
		return "Ready"
	case StatusError:
		return "Error"
	case StatusClosed:
		return "Closed"
	case StatusInit:
		return "Initialized"
	default:
		return "unknown"
	}
}

type Link struct {
	event LinkEventListener

	clientID string
	status   StatusType
	conn     net.Conn

	handlers map[string]*LinkDataHandler

	stopChan  chan struct{}
	incomming chan Payload // 有输入
	outgoing  chan Payload // 要输出

	datalock sync.Mutex
}

func newLink(conn net.Conn, event LinkEventListener) *Link {
	link := &Link{
		event:     event,
		conn:      conn,
		stopChan:  make(chan struct{}, 10),
		incomming: make(chan Payload),
		outgoing:  make(chan Payload),
	}
	link.fireStatusEvent(StatusInit)
	return link
}

func (l *Link) run() error {
	go l.readLoop()
	go l.writeLoop()
	go l.dataHandleLoop()
	return nil
}

func (l *Link) GetClientID() string {
	return l.clientID
}

func (l *Link) SetClientID(clientID string) {
	l.clientID = clientID
}

func (l *Link) GetStatus() StatusType {
	return l.status
}

func (l *Link) readLoop() {
	defer func() {
		l.Close()
		// BaseLink conn空指针异常保护
		if x := recover(); x != nil {
			logrus.Errorf("[%s] readLoop error: %s", l.GetClientID(), x)
		}
		logrus.Debug("read loop stopped.")
	}()
	// ----------- 报文交互 --------------
	dataBuffer := make([]byte, 0, 2048)
	payLoad := make([]byte, 0)
	splitLess := 0
	// 游标块长度 header + uint32
	cursorBlock := payloadHeaderLen + 4
	for {
		if l.status == StatusClosed {
			select {
			// 不可读, 稍等.
			case <-time.After(readWaitTime):
				continue
			// 退出信号，跳出等待
			case <-l.stopChan:
				return
			}
		}
		b := make([]byte, 512)
		err := l.conn.SetReadDeadline(time.Now().Add(KeepAliveTimeout))
		if err != nil {
			continue
		}
		recvLen, err := l.conn.Read(b)
		if err != nil {
			return
		}
		dataBuffer = append(dataBuffer, b[:recvLen]...)
		// 缺失报文补全处
		if splitLess > 0 {
			if len(dataBuffer) < splitLess {
				continue
			}
			payLoad = append(payLoad, dataBuffer[:splitLess]...)
			dataBuffer = dataBuffer[splitLess:]
			splitLess = 0
			recvLen = len(dataBuffer)
			// 报文接收完毕-2区
			if load, err := DecodePayLoad(payLoad); err == nil {
				l.fireStreamEvent(load, StreamIncoming)
				l.incomming <- load
			}
		}
		payLoad = make([]byte, 0)
		for i := 0; i < recvLen; i++ {
			if len(dataBuffer) >= cursorBlock && payloadHeader == string(dataBuffer[:payloadHeaderLen]) {
				payloadLen := int(binary.BigEndian.Uint32(dataBuffer[payloadHeaderLen:cursorBlock]))
				if payloadLen > len(dataBuffer) {
					splitLess = payloadLen - len(dataBuffer)
					payLoad = append(payLoad, dataBuffer...)
					dataBuffer = make([]byte, 0)
					break
				} else {
					payLoad = append(payLoad, dataBuffer[:payloadLen]...)
					dataBuffer = dataBuffer[payloadLen:]
					// 报文接收完毕-1区
					if load, err := DecodePayLoad(payLoad); err == nil {
						l.fireStreamEvent(load, StreamIncoming)
						l.incomming <- load
					}
				}
			}
			// 缓冲区大于游标尺寸开始左移
			if len(dataBuffer) > cursorBlock {
				dataBuffer = dataBuffer[1:]
			}
		}
	}
}

// readRegister 客户端注册
func (l *Link) readRegister() error {
	regPayload := <-l.incomming
	if regPayload.Type() != 0 {
		return errors.New("reg pkg type err")
	}
	l.clientID = string(regPayload.Bytes())
	// l.fireStatusEvent(StatusReady)
	return nil
}

// sendRegister 客户端注册
func (l *Link) sendRegister() error {
	payload, err := NewBytesPayload(0, []byte(l.GetClientID()))
	if err != nil {
		return err
	}
	l.outgoing <- payload
	l.fireStatusEvent(StatusReady)
	return nil
}

func (l *Link) writeLoop() {
	defer func() {
		l.Close()
		if x := recover(); x != nil {
			logrus.Errorf("writeLoop error: %s", x)
		}
	}()
	var loopBreaked bool
	for !loopBreaked {
		select {
		case p := <-l.outgoing:
			err := l.conn.SetWriteDeadline(time.Now().Add(KeepAliveTimeout))
			if err != nil {
				continue
			}
			l.fireStreamEvent(p, StreamOutgoing)
			raw, err := EncodePayLoad(p)
			if err != nil {
				continue
			}
			// TODO: 写入校验
			_, _ = l.conn.Write(raw)
		case <-l.stopChan:
			loopBreaked = true
			break
		}
	}
	logrus.Debugf("writeloop stopped.")
}

// ASend 异步发送数据
func (l *Link) ASend(payload Payload, handler func(Payload)) (err error) {
	if l.status != StatusReady {
		return errors.New("link status is unready")
	}
	if handler != nil {
		var destroy sync.WaitGroup
		destroy.Add(1)
		h := &LinkDataHandler{
			ack:      true,
			pkgType:  -payload.Type(),
			pkgIndex: payload.GetPkgIdx(),
			fn: func(p Payload) {
				defer func() {
					destroy.Done()
				}()
				handler(p)
			},
		}
		key := fmt.Sprintf("%v", h)
		l.AddDataHandler(key, h)
		go func() {
			destroy.Wait()
			l.RemoveDataHandler(key)
		}()
	}
	l.outgoing <- payload
	return
}

// SSend 同步发送.  等待一次应答.
func (l *Link) SSend(payload Payload, timeout time.Duration) (Payload, error) {
	if l.status != StatusReady {
		return nil, errors.New("link status is unready")
	}
	var (
		ack      Payload
		ok       bool
		err      error
		tchannel = make(chan Payload)
	)
	h := &LinkDataHandler{
		ack:      true,
		pkgType:  -payload.Type(),
		pkgIndex: payload.GetPkgIdx(),
		fn: func(p Payload) {
			tchannel <- p
		},
	}
	key := fmt.Sprintf("%v", h)
	l.AddDataHandler(key, h)
	defer l.RemoveDataHandler(key)
	l.outgoing <- payload
	// Read
	select {
	case ack, ok = <-tchannel:
		if !ok {
			err = fmt.Errorf("channel error")
		}
	case <-time.After(timeout):
		err = fmt.Errorf("read timeout:[%3.3fs]", timeout.Seconds())
	}
	return ack, err
}

func (l *Link) dataHandleLoop() {
	var loopBreaked bool
	for !loopBreaked {
		select {
		case p := <-l.incomming:
			if l.handlers == nil {
				continue
			}
			for _, handler := range l.handlers {
				// 正向返回
				if (handler.ack && p.Type() == handler.pkgType && handler.pkgIndex == p.GetPkgIdx()) ||
					(!handler.ack && (p.Type() == handler.pkgType || handler.pkgType == 0)) {
					go handler.fn(p)
				}
			}
		case <-l.stopChan:
			loopBreaked = true
			break
		}
	}
}

// AddDataHandler 设置异步数据回调处理接口.
func (l *Link) AddDataHandler(key string, handler *LinkDataHandler) {
	l.datalock.Lock()
	defer l.datalock.Unlock()
	if handler == nil {
		return
	}
	if l.handlers == nil {
		l.handlers = make(map[string]*LinkDataHandler)
	}
	// key := fmt.Sprintf("%v", &handler) //address
	// log.Debug("Link: Add data handler: %s", key)
	l.handlers[key] = handler
}

// RemoveDataHandler 设置异步数据回调处理接口.
func (l *Link) RemoveDataHandler(key string) {
	l.datalock.Lock()
	defer l.datalock.Unlock()
	if l.handlers == nil {
		return
	}
	// key := fmt.Sprintf("%v", &handler) //address
	delete(l.handlers, key)
}

// Close 接口方法, 关闭链接
func (l *Link) Close() {
	defer func() {
		if x := recover(); x != nil {
			logrus.Errorf("Close link error: %s", x)
		}
	}()
	l.fireStatusEvent(StatusClosed)
	for i := 0; i < 10 && len(l.stopChan) < 1; i++ {
		l.stopChan <- struct{}{}
	}
}

func (l *Link) fireStreamEvent(payload Payload, dir StreamDir) {
	l.event.OnStream(l, payload, dir)
}

func (l *Link) fireStatusEvent(stat StatusType) {
	l.status = stat
	l.event.OnStatus(l, stat)
}

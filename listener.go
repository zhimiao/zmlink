package zmlink

import (
	"fmt"
	"net"
	"sync"

	"github.com/sirupsen/logrus"
)

// StreamDir Stream方向类型
type StreamDir byte

const (
	// StreamIncoming Incoming
	StreamIncoming StreamDir = iota
	// StreamOutgoing Outgoing
	StreamOutgoing
)

// StreamDirString streamDir 转 string
func (dir StreamDir) String() string {
	switch dir {
	case StreamIncoming:
		return "Incoming"
	case StreamOutgoing:
		return "Outgoing"
	default:
		return "Unknown"
	}
}

type LinkEventListener interface {
	// OnStream 数据流 (源, 大小, 方向)
	OnStream(*Link, Payload, StreamDir)
	// OnStatus 状态变更(源,状态,附加信息)
	OnStatus(*Link, StatusType)
}

type Listener struct {
	Type              string // tcp4/udp
	Address           string // listen address
	linked            map[string]*Link
	listener          net.Listener
	linkEventListener LinkEventListener
	sync.Mutex        // connected 锁
}

func NewLinkListener(address string, linkEventListener LinkEventListener) *Listener {
	l := &Listener{
		Type:              "tcp",
		Address:           address,
		linked:            make(map[string]*Link),
		linkEventListener: linkEventListener,
	}
	return l
}

func (l *Listener) Start() error {
	tcpAddr, err := net.ResolveTCPAddr(l.Type, l.Address)
	if err != nil {
		return err
	}
	l.listener, err = net.ListenTCP(l.Type, tcpAddr)
	if err != nil {
		return err
	}
	go l.listening()
	return nil
}

// GetLink 获取连接
func (l *Listener) GetLink(ID string) *Link {
	if link, ok := l.linked[ID]; ok {
		return link
	}
	return nil
}

func (l *Listener) listening() {
	for {
		conn, err := l.listener.Accept()
		if err != nil { // listener 被 stop后抛出本异常.
			logrus.Errorf("[%s] Listening error: %s", l.string(), err.Error())
			break
		}
		logrus.Debugf("[%s]: new client: %s", l.string(), conn.RemoteAddr())
		go l.onNewConn(conn)
	}
	logrus.Debugf("[%s] stopped.", l.string())
}

// 监听到新链接.
func (l *Listener) onNewConn(conn net.Conn) {
	link := newLink(conn, l)
	err := link.run()
	if err != nil {
		return
	}
	// 注册帧识别
	if err := link.readRegister(); err != nil {
		link.Close()
		return
	}
	l.Lock()
	defer l.Unlock()
	l.linked[link.GetClientID()] = link
	link.fireStatusEvent(StatusReady)
}

// Stop 停止监听.
func (l *Listener) Stop() error {
	logrus.Debugf("[%s]: stopping...%d conns", l.string(), len(l.linked))
	l.Lock()
	defer l.Unlock()
	// 关闭已经打开的链接.
	for _, link := range l.linked {
		link.Close()
		delete(l.linked, link.GetClientID())
	}
	l.linked = nil
	err := l.listener.Close()
	return err
}

func (l *Listener) string() string {
	return fmt.Sprintf("%s://%s", l.Type, l.Address)
}

func (l *Listener) OnStream(link *Link, payload Payload, dir StreamDir) {
	l.linkEventListener.OnStream(link, payload, dir)
}

func (l *Listener) OnStatus(link *Link, statusType StatusType) {
	if statusType == StatusClosed {
		l.Lock()
		delete(l.linked, link.GetClientID())
		l.Unlock()
	}
	l.linkEventListener.OnStatus(link, statusType)
}
